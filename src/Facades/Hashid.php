<?php namespace Luminaire\Hashid\Facades;

/**
 * Created by Sublime Text 3
 *
 * @user     Kevin Tanjung
 * @website  http://kevintanjung.github.io
 * @email    kevin@rupawa.com
 * @date     15/12/2015
 * @time     14:51
 */

use Illuminate\Support\Facades\Facade;
use Hashids\Hashids;

/**
 * The Hashid Facade
 *
 * @package  \Luminaire\Hashid\Facades
 */
class Hashid extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     *
     * @throws \RuntimeException
     */
    protected static function getFacadeAccessor()
    {
        return Hashids::class;
    }

}
