# Luminaire Hashid

## Installation

Require this package, with Composer, in the root directory of your project.

```
composer require luminaire/hashid
```

Add the service provider to `config/app.php` in the providers array.

```
Luminaire\Hashid\HashidsServiceProvider::class
```

If you want you can use the facade. Add the reference in `config/app.php `to your aliases array.

```
'Hashid' => 'Luminaire\Hashid\Facades\Hashid'
```

## Configuration

```
php artisan vendor:publish
```

Configure as necessary, APP_KEY shouldn't be change though after encoding once.
