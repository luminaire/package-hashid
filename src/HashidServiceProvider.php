<?php namespace Luminaire\Hashid;

/**
 * Created by Sublime Text 3
 *
 * @user     Kevin Tanjung
 * @website  http://kevintanjung.github.io
 * @email    kevin@rupawa.com
 * @date     15/12/2015
 * @time     14:44
 */

use Illuminate\Support\ServiceProvider;
use Hashids\Hashids;

/**
 * The Hashid Service Provider
 *
 * @package  \Luminaire\Hashid
 */
class HashidServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
        $source = realpath(__DIR__ . '/../config/hashid.php');

        if ($this->app instanceof \Illuminate\Foundation\Application)
        {
            $this->publishes([$source => config_path('hashid.php')]);
        }
        elseif ($this->app instanceof \Laravel\Lumen\Application)
        {
            $this->app->configure('hashid');
        }

        $this->mergeConfigFrom($source, 'hashid');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Hashids::class, function(Application $app)
        {
            $config = $app['config'];

            $salt     = $config->get('hashid.salt', env('APP_KEY'));
            $length   = $config->get('hashid.length', 8);
            $alphabet = $config->get('hashid.alphabet', '');

            return new Hashids($salt, $length, $alphabet);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            Hashids::class
        ];
    }

}
